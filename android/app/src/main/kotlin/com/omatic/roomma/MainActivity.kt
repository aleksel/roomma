package com.omatic.roomma

import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.graphics.RectF
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.Toast
import com.alamkanak.weekview.DateTimeInterpreter
import com.alamkanak.weekview.MonthLoader
import com.alamkanak.weekview.WeekView
import com.alamkanak.weekview.WeekViewEvent
import com.example.roomma.R
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.text.SimpleDateFormat
import java.util.*


private const val TAG = "CALENDAR_NATIVE_VIEW"

class MainActivity() : FlutterActivity(), WeekView.EventClickListener, MonthLoader.MonthChangeListener,
        WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener {
    companion object {
        private var mWeekViewType = 1
        const val CHANNEL = "flutter.omatic.com.channel"
    }

    var layoutParams: FrameLayout.LayoutParams? = null
    var dialog: ImmersiveDialogFragment? = null
    var isAuth = false

    override fun onUserLeaveHint() {
        if (!isAuth) {
            startActivity(Intent(this@MainActivity, MainActivity::class.java))
            finish()
        }
        super.onUserLeaveHint()
    }

    /*override fun onAttachedToWindow() {
        this.window.setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG)
        super.onAttachedToWindow()
    }*/

    private fun open(view: View) {
        // build alert dialog
        dialog = ImmersiveDialogFragment()
        dialog!!.showImmersive(this)
    }

    fun onAuthUser(selectedValue: String) {
        val data = HashMap<String, Any>()
        data["password"] = selectedValue
        MethodChannel(flutterView, CHANNEL).invokeMethod("adminAuthCheckPass", data)
    }

    fun onDestroyDialog() {
        dialog = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        //window.requestFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        //his.requestWindowFeature(Window.FEATURE_NO_TITLE) // Remove title bar
        // Remove notification bar
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            run {
                Log.v(TAG, visibility.toString())
                window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_IMMERSIVE)
            }
        }
        GeneratedPluginRegistrant.registerWith(this)
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE)

        //window.addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY)
        MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
            when {
                call.method == "close" -> {
                    close(call, result)
                }
                call.method == "resizeCalendarView" -> {
                    resize(call, result)
                }
                call.method == "initCalendarCallbackHack" -> {
                    if (weekView != null)
                        weekView!!.goToHour(GregorianCalendar.getInstance().get(Calendar.HOUR_OF_DAY).toDouble())
                }
                call.method == "changeNativeViewState" -> {
                    changeNativeViewState(call, result)
                }
                call.method == "showCalendarView" -> {
                    showNativeView(call, result, mWeekViewType)
                }
                call.method == "adminLogout" -> {
                    isAuth = false
                }
                call.method == "adminAuth" -> {
                    open(flutterView)
                }
                call.method == "adminAuthSuccess" -> {
                    Log.v(TAG, "adminAuthSuccess")
                    isAuth = true
                    dialog!!.destroyDialog()
                    Toast.makeText(this, "Authenticated successfully", Toast.LENGTH_SHORT).show()
                }
                call.method == "adminAuthFailed" -> {
                    val error: String = call.argument("error")
                    Log.v(TAG, "adminAuthFailed: $error")
                    dialog!!.setError(error)
                }
            }
        }
    }

    private fun showNativeView(call: MethodCall, result: MethodChannel.Result, dayCount: Int) {
        val calendar = GregorianCalendar.getInstance()
        Log.v(TAG, "showNativeView: " + calendar.get(Calendar.HOUR_OF_DAY).toDouble())
        if (layoutParams == null) {
            layoutParams = buildLayoutParams(call)
        }
        val params = layoutParams
        weekView = WeekView(this)
        //mWeekView = (WeekView) findViewById(R.id.weekView);
        weekView!!.numberOfVisibleDays = dayCount
        weekView!!.monthChangeListener = this
        //setContentView(weekView)
        weekView!!.bringToFront()
        weekView!!.firstDayOfWeek = Calendar.MONDAY
        weekView!!.hourHeight = 100
        /* weekView!!.setOn { visibility ->
             run {
                 Log.v(TAG, "CALENDAR VISIBILITY: $visibility")
             }
         }*/
        weekView!!.isShowNowLine = true
        //addContentView(flutterView, params)
        addContentView(weekView, params)

        weekView!!.goToHour(calendar.get(Calendar.HOUR_OF_DAY).toDouble())
        /*val intent = Intent(this, MainActivity::class.java)
        intent.addCategory(Intent.CATEGORY_HOME)
        startActivity(intent)*/
        setupDateTimeInterpreter(false)
        result.success(true)
    }

    private fun changeNativeViewState(call: MethodCall, result: MethodChannel.Result) {
        val state: Int = call.argument("state")
        if (state == 0) {
            mWeekViewType = 1
        } else if (state == 1) {
            mWeekViewType = 3
        }
        Log.v(TAG, state.toString())
        Log.v(TAG, mWeekViewType.toString())
        Log.v(TAG, (weekView == null).toString())
        when {
            state == 2 -> close(null, result)
            weekView == null -> showNativeView(call, result, mWeekViewType)
            state != mWeekViewType -> {
                if (state == 0) {
                    weekView!!.numberOfVisibleDays = 1
                    // Lets change some dimensions to best fit the view.
                    weekView!!.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
                    weekView!!.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics).toInt()
                    weekView!!.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics).toInt()
                } else if (state == 1) {
                    weekView!!.numberOfVisibleDays = 3
                    weekView!!.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
                    weekView!!.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics).toInt()
                    weekView!!.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics).toInt()
                }
                result.success(true)
            }
        }
    }

    private fun close(call: MethodCall?, result: MethodChannel.Result) {
        if (weekView != null) {
            val vg = weekView!!.parent as ViewGroup
            vg.removeView(weekView)
        }
        weekView = null
        result.success(null)

        //FlutterWebviewPlugin.channel.invokeMethod("onDestroy", null)
    }

    private fun resize(call: MethodCall, result: MethodChannel.Result) {
        val params = buildLayoutParams(call)
        weekView!!.layoutParams = params
        result.success(true)
    }

    var weekView: WeekView? = null

    private fun buildLayoutParams(call: MethodCall): FrameLayout.LayoutParams {
        val rc = call.argument<Map<String, Number>>("rect")
        val params: FrameLayout.LayoutParams
        if (rc != null) {
            params = FrameLayout.LayoutParams(
                    dp2px(this, rc["width"]!!.toFloat()), dp2px(this, rc["height"]!!.toFloat()))
            params.setMargins(dp2px(this, rc["left"]!!.toFloat()), dp2px(this, rc["top"]!!.toFloat()),
                    0, 0)
        } else {
            val display = this.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val width = size.x
            val height = size.y
            params = FrameLayout.LayoutParams(width, height)
        }

        return params
    }

    private fun dp2px(context: Context, dp: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    private fun setupDateTimeInterpreter(shortDate: Boolean) {
        weekView!!.dateTimeInterpreter = object : DateTimeInterpreter {
            override fun interpretDate(date: Calendar): String {
                val weekdayNameFormat = SimpleDateFormat("EEE", Locale.getDefault())
                var weekday = weekdayNameFormat.format(date.time)
                val format = SimpleDateFormat(" M/d", Locale.getDefault())

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = weekday[0].toString()
                return weekday.toUpperCase() + format.format(date.time)
            }

            override fun interpretTime(hour: Int): String {
                return if (hour > 9) hour.toString() + ":00" else if (hour == 0) "00:00" else "0$hour:00"
            }
        }
    }

    private fun getEventTitle(time: Calendar): String {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH))
    }

    override fun onEventClick(event: WeekViewEvent, eventRect: RectF) {
        //Toast.makeText(this, "Clicked " + event.name, Toast.LENGTH_SHORT).show()
    }

    override fun onEventLongPress(event: WeekViewEvent, eventRect: RectF) {
        // Toast.makeText(this, "Long pressed event: " + event.name, Toast.LENGTH_SHORT).show()
    }

    override fun onEmptyViewLongPress(time: Calendar) {
        //Toast.makeText(this, "Empty view long pressed: " + getEventTitle(time), Toast.LENGTH_SHORT).show()
    }

    override fun onMonthChange(newYear: Int, newMonth: Int): List<WeekViewEvent> {
        // Populate the week view with some events.
        val events = ArrayList<WeekViewEvent>()

        var startTime = Calendar.getInstance()
        startTime.set(Calendar.HOUR_OF_DAY, 3)
        startTime.set(Calendar.MINUTE, 0)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        var endTime = startTime.clone() as Calendar
        endTime.add(Calendar.HOUR, 1)
        endTime.set(Calendar.MONTH, newMonth - 1)
        var event = WeekViewEvent(1, getEventTitle(startTime), startTime, endTime)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_01)
        events.add(event)

        startTime = Calendar.getInstance()
        startTime.set(Calendar.HOUR_OF_DAY, 3)
        startTime.set(Calendar.MINUTE, 30)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        endTime = startTime.clone() as Calendar
        endTime.set(Calendar.HOUR_OF_DAY, 4)
        endTime.set(Calendar.MINUTE, 30)
        endTime.set(Calendar.MONTH, newMonth - 1)
        event = WeekViewEvent(10, getEventTitle(startTime), startTime, endTime)
        //event.color = resources.getColor(R.color.event_color_02)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_02)
        events.add(event)

        startTime = Calendar.getInstance()
        startTime.set(Calendar.HOUR_OF_DAY, 4)
        startTime.set(Calendar.MINUTE, 20)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        endTime = startTime.clone() as Calendar
        endTime.set(Calendar.HOUR_OF_DAY, 5)
        endTime.set(Calendar.MINUTE, 0)
        event = WeekViewEvent(10, getEventTitle(startTime), startTime, endTime)
        // event.color = resources.getColor(R.color.event_color_03)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_03)
        events.add(event)

        startTime = Calendar.getInstance()
        startTime.set(Calendar.HOUR_OF_DAY, 5)
        startTime.set(Calendar.MINUTE, 30)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        endTime = startTime.clone() as Calendar
        endTime.add(Calendar.HOUR_OF_DAY, 2)
        endTime.set(Calendar.MONTH, newMonth - 1)
        event = WeekViewEvent(2, getEventTitle(startTime), startTime, endTime)
        //event.color = resources.getColor(R.color.event_color_02)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_02)
        events.add(event)

        startTime = Calendar.getInstance()
        startTime.set(Calendar.HOUR_OF_DAY, 5)
        startTime.set(Calendar.MINUTE, 0)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        startTime.add(Calendar.DATE, 1)
        endTime = startTime.clone() as Calendar
        endTime.add(Calendar.HOUR_OF_DAY, 3)
        endTime.set(Calendar.MONTH, newMonth - 1)
        event = WeekViewEvent(3, getEventTitle(startTime), startTime, endTime)
        //event.color = resources.getColor(R.color.event_color_03)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_03)
        events.add(event)

        startTime = Calendar.getInstance()
        startTime.set(Calendar.DAY_OF_MONTH, 15)
        startTime.set(Calendar.HOUR_OF_DAY, 3)
        startTime.set(Calendar.MINUTE, 0)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        endTime = startTime.clone() as Calendar
        endTime.add(Calendar.HOUR_OF_DAY, 3)
        event = WeekViewEvent(4, getEventTitle(startTime), startTime, endTime)
        // event.color = resources.getColor(R.color.event_color_04)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_04)
        events.add(event)

        startTime = Calendar.getInstance()
        startTime.set(Calendar.DAY_OF_MONTH, 1)
        startTime.set(Calendar.HOUR_OF_DAY, 3)
        startTime.set(Calendar.MINUTE, 0)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        endTime = startTime.clone() as Calendar
        endTime.add(Calendar.HOUR_OF_DAY, 3)
        event = WeekViewEvent(5, getEventTitle(startTime), startTime, endTime)
        //event.color = resources.getColor(R.color.event_color_01)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_01)
        events.add(event)

        startTime = Calendar.getInstance()
        startTime.set(Calendar.DAY_OF_MONTH, startTime.getActualMaximum(Calendar.DAY_OF_MONTH))
        startTime.set(Calendar.HOUR_OF_DAY, 15)
        startTime.set(Calendar.MINUTE, 0)
        startTime.set(Calendar.MONTH, newMonth - 1)
        startTime.set(Calendar.YEAR, newYear)
        endTime = startTime.clone() as Calendar
        endTime.add(Calendar.HOUR_OF_DAY, 3)
        event = WeekViewEvent(5, getEventTitle(startTime), startTime, endTime)
        //event.color = resources.getColor(R.color.event_color_02)
        event.color = ContextCompat.getColor(applicationContext, R.color.event_color_02)
        events.add(event)

        return events
    }
}
