package com.omatic.roomma

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.WindowManager
import android.widget.EditText
import com.example.roomma.R


class ImmersiveDialogFragment : DialogFragment() {
    private var editField: EditText? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_Alert)
                .setTitle("Authentication")
        //.setMessage("Some text.")

        val dialogView = activity.layoutInflater.inflate(R.layout.custom_dialog, null)
        editField = dialogView.findViewById(R.id.edit1)
        editField!!.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                editField!!.error = null
            }
        })

        builder.setView(dialogView)
        // Set up the buttons
        builder.setPositiveButton("OK", null)
        builder.setNegativeButton("Cancel", { dialogInterface, position -> onCancelDialog() })
        val alertDialog = builder.create()
        alertDialog.setOnShowListener({
            val b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            b.setOnClickListener {
                onClick()
            }
        })
        // Temporarily set the dialogs window to not focusable to prevent the short
        // popup of the navigation bar.
        alertDialog.window.addFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)

        return alertDialog
    }

    private fun onCancelDialog() {
        Log.v("SSS", "onCancelDialog")
        val callingActivity = activity as MainActivity
        callingActivity.onDestroyDialog()
    }

    fun destroyDialog() {
       dialog.dismiss()
    }

    fun setError(error: String) {
        Log.v("SSS", error)
        editField!!.error = error
    }

    private fun onClick() {
        val value = editField!!.text.toString()
        val callingActivity = activity as MainActivity
        callingActivity.onAuthUser(value)
        //dialog.dismiss()
    }

    fun showImmersive(activity: Activity) {

        // Show the dialog.
        show(activity.fragmentManager, null)

        // It is necessary to call executePendingTransactions() on the FragmentManager
        // before hiding the navigation bar, because otherwise getWindow() would raise a
        // NullPointerException since the window was not yet created.
        fragmentManager.executePendingTransactions()

        // Hide the navigation bar. It is important to do this after show() was called.
        // If we would do this in onCreateDialog(), we would get a requestFeature()
        // error.
        dialog.window.decorView.systemUiVisibility = activity.window.decorView.systemUiVisibility

        // Make the dialogs window focusable again.
        dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)

    }

}
