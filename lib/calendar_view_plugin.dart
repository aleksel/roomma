import 'dart:async';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:roomma/utils/constatnt.dart';

class CalendarViewPlugin {
  static const MethodChannel _channel = const MethodChannel(mainChannel);
  Function _onSetIsAuth;

  Future<Null> launch(String url, {Rect rect}) async {
    Map<String, dynamic> args = {};
    if (rect != null) {
      args["rect"] = {"left": rect.left, "top": rect.top, "width": rect.width, "height": rect.height};
    }
    await _channel.invokeMethod(showCalendarView, args);
    await _channel.invokeMethod(initCalendarCallbackHack);
  }

  /// resize webview
  Future<Null> resize(Rect rect) async {
    final args = {};
    args["rect"] = {"left": rect.left, "top": rect.top, "width": rect.width, "height": rect.height};
    await _channel.invokeMethod(resizeCalendarView, args);
  }

  CalendarViewPlugin() {
    _channel.setMethodCallHandler(_handleMessages);
  }

  void setOnSetIsAuth(onSetIsAuth) {
    this._onSetIsAuth = onSetIsAuth;
  }

  Future<Null> _handleMessages(MethodCall call) async {
    switch (call.method) {
      case adminAuthCheckPass:
        String password = call.arguments["password"];
        if (password != null && password == "123") {
          _authSuccess();
          this._onSetIsAuth(true);
        } else {
          _authFailed();
        }
        break;
    }
  }

  Future<Null> _authSuccess() async {
    await _channel.invokeMethod(adminAuthSuccess);
  }

  Future<Null> _authFailed() async {
    await _channel.invokeMethod(adminAuthFailed,  {'error': authFailed});
  }
}
