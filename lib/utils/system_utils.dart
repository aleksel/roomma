import 'package:flutter/widgets.dart';

final dismissKeyboard = (context) => FocusScope.of(context).requestFocus(new FocusNode());
final getKeyboardHeight = (context) => MediaQuery.of(context).viewInsets.bottom;

