const MAIN_PAGE = "/";
const DETAIL_PAGE = "detail_page";

const LOGOUT_TIMEOUT = 5;
const GO_BACK_MAIN_SCREEN_TIMEOUT = 5;

// ===== RIGHT BUTTONS =====
const oneDay = '1 DAY';
const threeDay = '3 DAYS';
const state = 'STATE';

// ===== NATIVE CHANNEL =====
const mainChannel = 'flutter.omatic.com.channel';

// ===== ERRORS =====
const authFailed = 'Password is incorrect';

// ===== NATIVE EVENTS =====
const initCalendarCallbackHack = 'initCalendarCallbackHack';
const changeState = 'changeNativeViewState';
const showCalendarView = 'showCalendarView';
const resizeCalendarView = 'resizeCalendarView';
const adminAuth = 'adminAuth';
const adminLogout = 'adminLogout';

const adminAuthCheckPass = 'adminAuthCheckPass';
const adminAuthSuccess = 'adminAuthSuccess';
const adminAuthFailed = 'adminAuthFailed';
