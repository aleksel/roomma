import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:roomma/calendar_view_plugin.dart';

class CalendarViewScaffold extends StatefulWidget {
  final PreferredSizeWidget appBar;
  final String url;
  final bool primary;
  final Function onSetIsAuth;
  final List<Widget> persistentFooterButtons;
  final Widget bottomNavigationBar;

  CalendarViewScaffold(
      {Key key,
      this.onSetIsAuth,
      this.appBar,
      @required this.url,
      this.primary: true,
      this.persistentFooterButtons,
      this.bottomNavigationBar})
      : super(key: key);

  @override
  _CalendarViewScaffoldState createState() => new _CalendarViewScaffoldState();
}

class _CalendarViewScaffoldState extends State<CalendarViewScaffold> {
  final calendarViewReference = new CalendarViewPlugin();
  Rect _rect;
  Timer _resizeTimer;

  void initState() {
    super.initState();
    calendarViewReference.setOnSetIsAuth(widget.onSetIsAuth);
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_rect == null) {
      _rect = _buildRect(context);
      calendarViewReference.launch(widget.url, rect: _rect);
    } else {
      Rect rect = _buildRect(context);
      if (_rect != rect) {
        _rect = rect;
        _resizeTimer?.cancel();
        _resizeTimer = new Timer(new Duration(milliseconds: 300), () {
          // avoid resizing to fast when build is called multiple time
          calendarViewReference.resize(_rect);
        });
      }
    }
    return new Container(/*child: new Center(child: new CircularProgressIndicator())*/);
  }

  Rect _buildRect(BuildContext context) {
    bool fullscreen = widget.appBar == null;

    final mediaQuery = MediaQuery.of(context);
    final topPadding = widget.primary ? mediaQuery.padding.top : 0.0;
    num top = fullscreen ? 0.0 : widget.appBar.preferredSize.height + topPadding;

    num height = mediaQuery.size.height - top;

    if (widget.bottomNavigationBar != null) {
      height -= 56.0 +
          mediaQuery.padding.bottom; // todo(lejard_h) find a way to determine bottomNavigationBar programmatically
    }

    if (widget.persistentFooterButtons != null) {
      height -= 53.0; // todo(lejard_h) find a way to determine persistentFooterButtons programmatically
      if (widget.bottomNavigationBar == null) {
        height -= mediaQuery.padding.bottom;
      }
    }

    return new Rect.fromLTWH(0.0, top, mediaQuery.size.width - 110,  mediaQuery.size.height);
  }
}
