import 'dart:async';

import 'package:flutter/material.dart';
import 'package:roomma/utils/constatnt.dart';
import 'package:roomma/utils/image_utils.dart';

class DetailScreen extends StatefulWidget {
  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  Timer _backTimer;

  @override
  void initState() {
    super.initState();
    if (_backTimer != null) {
      _backTimer.cancel();
    }
    _backTimer = new Timer(const Duration(minutes: GO_BACK_MAIN_SCREEN_TIMEOUT), () {
      _backTimer = null;
      Navigator.of(context).pop('on back');
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GestureDetector(
          onTap: () => Navigator.of(context).pop('on back'),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: Container(
                  //color: Colors.amberAccent,
                  child: Stack(children: [
                    Opacity(
                      child: new Image.asset(
                        ImageAssets.negotiationRoom,
                        gaplessPlayback: true,
                        alignment: Alignment.topCenter,
                        height: double.infinity,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      ),
                      opacity: 0.6,
                    ),
                    Container(
                      color: Color.fromRGBO(0, 0, 0, 0.6),
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            child: Text(
                              "GET A ROOM CONFERENCE ROOM BOOKING",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w700),
                            ),
                            padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
                          ),
                          Expanded(
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Samara Room",
                                    style:
                                        TextStyle(color: Colors.white70, fontWeight: FontWeight.w400, fontSize: 22.0),
                                  ),
                                  Text(
                                    "Booked",
                                    style:
                                        TextStyle(color: Colors.white70, fontWeight: FontWeight.w100, fontSize: 58.0),
                                  ),
                                  Text(
                                    "By James Smith from 10:00 - 11:00",
                                    style:
                                        TextStyle(color: Colors.white70, fontWeight: FontWeight.w400, fontSize: 14.0),
                                  ),
                                  Text(
                                    "Then free until 13:30",
                                    style:
                                        TextStyle(color: Colors.white70, fontWeight: FontWeight.w400, fontSize: 14.0),
                                  ),
                                ],
                              ),
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(top: 44.0, left: 16.0, right: 16.0),
                            ),
                          ),
                          Padding(
                            child: Text(
                              "To book this room visit site: http://roomma.alx",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w400),
                            ),
                            padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 16.0),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
                flex: 5,
              ),
              Expanded(
                child: Container(
                  color: Color.fromRGBO(217, 49, 49, 1.0),
                  padding: EdgeInsets.only(top: 32.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTime(),
                        style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w300, fontSize: 58.0),
                      ),
                      Padding(
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: Container(
                            padding: EdgeInsets.only(bottom: 8.0),
                            decoration:
                                BoxDecoration(border: Border(bottom: BorderSide(width: 1.0, color: Colors.white70))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Bookings",
                                  style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w800, fontSize: 14.0),
                                ),
                                Text(
                                  "July " + DateTime.now().day.toString() + ", 2018",
                                  style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w800, fontSize: 14.0),
                                )
                              ],
                            ),
                          ),
                        ),
                        padding: EdgeInsets.only(top: 32.0),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "James Smith",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w300, fontSize: 14.0),
                            ),
                            Text(
                              "10:00 - 11:00",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w300, fontSize: 14.0),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Kevin Bacon",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w300, fontSize: 14.0),
                            ),
                            Text(
                              "13:30 - 14:00",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w300, fontSize: 14.0),
                            )
                          ],
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Sarah Mclachlan",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w300, fontSize: 14.0),
                            ),
                            Text(
                              "16:00 - 15:00",
                              style: TextStyle(color: Colors.white70, fontWeight: FontWeight.w300, fontSize: 14.0),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                flex: 3,
              ),
            ],
          )),
    );
  }

  String getTime() {
    final minutes = DateTime.now().minute;
    return DateTime.now().hour.toString() + ":" + (minutes < 10 ? ("0" + minutes.toString()) : minutes.toString());
  }

  @override
  void dispose() {
    super.dispose();
    if (_backTimer != null) {
      _backTimer.cancel();
    }
  }
}
