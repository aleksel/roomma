import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:roomma/calendar_view_scaffold.dart';
import 'package:roomma/detail_screen.dart';
import 'package:roomma/utils/constatnt.dart';

void main() {
  SystemChrome.setEnabledSystemUIOverlays([]);
  SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]).then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Roomma',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      initialRoute: MAIN_PAGE,
      routes: {
        // When we navigate to the "/" route, build the FirstScreen Widget
        MAIN_PAGE: (context) => MyHomePage(title: 'Main screen'),
        // When we navigate to the "/second" route, build the SecondScreen Widget
        DETAIL_PAGE: (context) => DetailScreen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

const _startFontSize = 16.0;
const _endFontSize = 18.0;
const _buttonsCount = 4;
const _buttonWidth = 65.0;
const _indicatorPositionOffset = 2 / (_buttonsCount - 1);
const buttons = [oneDay, threeDay, state];

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  static const platform = const MethodChannel(mainChannel);
  static Timer logOutTimer;
  int _index = 0;
  int _previousIndex = 0;
  bool isAuth = false;
  AnimationController _controller;
  Animation<double> _animationPosition;

  void setIsAuth(bool isAuth) {
    if (isAuth) {
      if (logOutTimer != null) {
        logOutTimer.cancel();
      }
      logOutTimer = new Timer(const Duration(minutes: LOGOUT_TIMEOUT), () {
        logOutTimer = null;
        setIsAuth(false);
      });
    }
    setState(() {
      this.isAuth = isAuth;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _controller = new AnimationController(vsync: this, duration: const Duration(milliseconds: 300));
    _animationPosition = new Tween<double>(
      begin: _startFontSize,
      end: _endFontSize,
    ).animate(new CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ));
    _animationPosition.addListener(() {
      this.setState(() {});
    });
    _animationPosition.addStatusListener((status) {
      if (status == AnimationStatus.completed) _controller.reset();
    });
  }

  @override
  didPopRoute() {
    return new Future<bool>.value(true);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  double _getFontSize(int buttonIndex) {
    if (_controller.isAnimating) {
      if (_index == buttonIndex) return _animationPosition.value;
      if (_previousIndex == buttonIndex) {
        return _startFontSize - _animationPosition.value + _endFontSize;
      }
      return _startFontSize;
    }
    if (_index == buttonIndex) return _endFontSize;
    return _startFontSize;
  }

  double _getIndicatorPosition() {
    final newPosition = -1 + (_index * _indicatorPositionOffset);
    final oldPosition = -1 + (_previousIndex * _indicatorPositionOffset);

    return _controller.isAnimating ? oldPosition + ((newPosition - oldPosition) * _controller.value) : newPosition;
  }

  void onTypeOnTab(int buttonIndex, BuildContext context) {
    setState(() {
      _controller.forward();
      _previousIndex = _index;
      _index = buttonIndex;
    });
    _changeNativeViewState(buttonIndex);
    if (buttonIndex == 2) {
      Navigator.of(context).pushNamed(DETAIL_PAGE).then((value) {
        new Timer(Duration(milliseconds: 350), () => _changeNativeViewState(0));
        setState(() {
          _controller.forward();
          _previousIndex = buttonIndex;
          _index = 0;
        });
      });
      /*showDialog(context: context, builder: (_) => new SimpleDialog(
        title: new Text("AUTH"),
      ));*/
    }
  }

  @override
  Widget build(BuildContext context) {
    final buttonHeight = MediaQuery.of(context).size.height / _buttonsCount;
    var currentIndex = -1;
    final currentButtons = buttons.map((text) {
      ++currentIndex;
      final index = currentIndex;
      return FlatButton(
        child: Container(
          child: Center(
              child: new Text(
            text,
            style: TextStyle(fontSize: _getFontSize(index)),
          )),
          height: buttonHeight,
          width: _buttonWidth,
          decoration: BoxDecoration(border: BorderDirectional(bottom: BorderSide(color: Colors.black38, width: 1.0))),
        ),
        onPressed: _index != index ? () => onTypeOnTab(index, context) : null,
      ) as Widget;
    }).toList();
    currentButtons.add(new Container(
      alignment: const Alignment(3.0, 1.0),
      width: _buttonWidth + 35,
      height: buttonHeight,
      child: FlatButton(
        padding: EdgeInsets.all(12.0),
        onPressed: _callAuthDialog,
        shape: CircleBorder(),
        child: Icon(
          isAuth ? Icons.exit_to_app : Icons.lock_open,
          size: 30.0,
          color: isAuth ? Colors.black87 : Colors.transparent,
        ),
      ),
    ));
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      primary: true,
      body: new Center(
        child: new Stack(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: [
            /*new RaisedButton(
              child: new Text('Show native view'),
              onPressed: _showNativeView,
            ),*/
            new Container(
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Container(),
                  ),
                  new Container(
                    child: Row(
                      children: <Widget>[
                        new Container(
                          width: 4.0,
                          height: double.infinity,
                          alignment: Alignment(1.0, _getIndicatorPosition()),
                          child: Container(
                            decoration: BoxDecoration(color: Color.fromRGBO(96, 125, 139, 0.4)),
                            height: buttonHeight,
                          ),
                        ),
                        new Column(
                          children: currentButtons,
                        )
                      ],
                    ),
                    margin: new EdgeInsets.only(left: 46.0),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10.0,
                          offset: new Offset(0.0, 10.0),
                        ),
                      ],
                      color: Colors.white,
                    ),
                    width: 105.0,
                    height: double.infinity,
                  ),
                ],
              ),
            ),
            new CalendarViewScaffold(
              onSetIsAuth: this.setIsAuth,
            ),
          ],
        ),
      ),
    );
  }

  Future<Null> _changeNativeViewState(int currentState) async {
    await platform.invokeMethod(changeState, {'state': currentState});
    await platform.invokeMethod(initCalendarCallbackHack, {'state': currentState});
  }

  Future<Null> _callAuthDialog() async {
    if (!isAuth) {
      await platform.invokeMethod(adminAuth);
    } else {
      setState(() {
        isAuth = false;
      });
      await platform.invokeMethod(adminLogout);
    }
  }
}
